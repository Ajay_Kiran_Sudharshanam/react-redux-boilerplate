import {
    splitter,
    validate,
    isAllFieldsValid,
    getFormFieldsValidated
} from './sharedUtils'

it("splitter", () => {
    const component = splitter("max:10")
    expect(component).toEqual({
        rule: "max",
        subRule: "10"
    })
})

it("validate with rule required and no values and fieldName", () => {
    const component = validate(["required"])
    expect(component).toEqual(" is a required field")
})

it("validate with rule max:10 with valid value", () => {
    const component = validate(["max:10"], "1234567892", "Contact")
    expect(component).toEqual("")
})

it("validate with rule max:10 with invalid value", () => {
    const component = validate(["max:10"], "123456789", "Contact")
    expect(component).toEqual("")
})

it("validate with rule min:10 with invalid value", () => {
    const component = validate(["min:10"], "1234", "Contact")
    expect(component).toEqual("Contact must be 10 or more characters.")
})

it("validate with rule min:10 with valid value", () => {
    const component = validate(["min:10"], "1234567890", "Contact")
    expect(component).toEqual("")
})

it("validate with rule required with fieldValue and no value", () => {
    const component = validate(["required"], "", "First Name")
    expect(component).toEqual("First Name is a required field")
})

it("validate with rule required with fieldValue and with value", () => {
    const component = validate(["required"], "test", "First Name")
    expect(component).toEqual("")
})

it("validate with rule email with invalid email", () => {
    const component = validate(["email"], "ivalid!", "Email")
    expect(component).toEqual(" Email should be in Email@Email.com format")
})

it("validate with rule email with valid email", () => {
    const component = validate(["email"], "valid@valid.com", "Email")
    expect(component).toEqual("")
})

it("isAllFieldsValid with missing fields", () => {
    const data = {
        firstName: "",
        lastName: "test last name"
    }
    const component = isAllFieldsValid(data)
    expect(component).toBe(true)
})

it("getFormFieldsValidated", () => {
    const ruleSet = {
        firstName: ["required", "min:3"],
        lastName: ["required", "min:3"],
        email: ["required", "email"],
        contactNumber: ["required", "min:10", "max:10"]
    }
    const values = {
        firstName: "test name",
        lastName: "test name",
        email: "inValid!",
        contactNumber: ""
    }
    const component = getFormFieldsValidated(ruleSet, values, "This")
    expect(component).toEqual({
        contactNumber: "This is a required field",
        email: " This should be in This@This.com format",
        firstName: "",
        lastName: ""
    })
})