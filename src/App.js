import React from 'react';
import logo from './utils/icons/concordlogo.jpg';

import Todos from './containers/Todos'

import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="Header-logo" alt="logo" />
      </header>
      <Todos />
    </div>
  );
}

export default App;
