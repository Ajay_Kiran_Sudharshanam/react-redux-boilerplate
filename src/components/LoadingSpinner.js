import React from 'react'

export function LoadingSpinner({ show, loadingMessage }) {
    if (show) {
        return (
            <div className="Loading">
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
                {loadingMessage && <h4 className="Title">{loadingMessage}</h4>}
                {!loadingMessage && <h4 className="Title">Loading..</h4>}
            </div>
        )
    }
    return null
}

export default LoadingSpinner