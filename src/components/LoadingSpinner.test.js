import React from 'react';
import { shallow } from 'enzyme';
import LoadingSpinner from './LoadingSpinner';

it('renders without crashing', () => {
    const props = {
        show: true
    }
    const component = shallow(<LoadingSpinner {...props} />);
    expect(component).toBeDefined()
});

it('renders without crashing show === true && loadingMessage', () => {
    const props = {
        show: true,
        loadingMessage: "test loading message"
    }
    const component = shallow(<LoadingSpinner {...props} />);
    expect(component).toBeDefined()
});

it('renders without crashing -- show === false', () => {
    const props = {
        show: false
    }
    const component = shallow(<LoadingSpinner {...props} />);
    expect(component).toBeDefined()
});
