import React from 'react'
import submitted from '../utils/icons/formsubmitted.png';

export function FormSubmitted() {
    return (
        <div className="FormSubmitted">
            <img src={submitted} className="Icon" alt="submitted" />
            <h2 className="Title">Submitted Successfully</h2>
        </div>
    )
}

export default FormSubmitted