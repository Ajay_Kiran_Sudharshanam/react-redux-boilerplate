import React from 'react';
import { shallow } from 'enzyme';
import FormSubmitted from './FormSubmitted';

it('renders without crashing', () => {
  const component = shallow(<FormSubmitted />);
  expect(component).toBeDefined()
});