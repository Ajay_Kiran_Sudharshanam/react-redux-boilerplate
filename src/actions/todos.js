export const TODOS_FORM_DETAILS_CHANGE = "TODOS_FORM_DETAILS_CHANGE"
export function todosFormDetailsChange(changedValue) {
    return {
        type: TODOS_FORM_DETAILS_CHANGE,
        changedValue
    }
}

export const VALIDATE_TODOS_FORM_FIELDS = "VALIDATE_TODOS_FORM_FIELDS"
export function validateTodosFormFields(fieldName, fieldValue) {
    return {
        type: VALIDATE_TODOS_FORM_FIELDS,
        fieldName,
        fieldValue
    }
}

export const ON_SUBMIT_VALIDATE_FIELDS = "ON_SUBMIT_VALIDATE_FIELDS"
export function onSubmitValidateFields(error) {
    return {
        type: ON_SUBMIT_VALIDATE_FIELDS,
        error
    }
}

export const FETCH_USER_DETAILS_STARTED = "FETCH_USER_DETAILS_STARTED"
export function fetchUserDetailsStarted() {
    return {
        type: FETCH_USER_DETAILS_STARTED
    }
}

export const FETCH_USER_DETAILS_SUCCESS = "FETCH_USER_DETAILS_SUCCESS"
export function fetchUserDetailsSuccess(details) {
    return {
        type: FETCH_USER_DETAILS_SUCCESS,
        details
    }
}

export const FETCH_USER_DETAILS_FAILED = "FETCH_USER_DETAILS_FAILED"
export function fetchUserDetailsFailed(error) {
    return {
        type: FETCH_USER_DETAILS_FAILED,
        error
    }
}

export function getDetails() {
    return dispatch => {
        dispatch(fetchUserDetailsStarted())
        return fetch('http://localhost:3001/userDetails')
            .then(res => res.json())
            // just to see a delay in the api call, I will use setTimeout
            // we dont need setTimeout in real env
            .then(body => setTimeout(() => {
                dispatch(fetchUserDetailsSuccess(body));
            }, 3000))
            .catch(error => dispatch(fetchUserDetailsFailed(error)))
    }
}

export const POST_USER_DETAILS_SUCCESS = "POST_USER_DETAILS_SUCCESS"
export function postUserDetailsSuccess() {
    return {
        type: POST_USER_DETAILS_SUCCESS
    }
}

export const POST_USER_DETAILS_FAILED = "POST_USER_DETAILS_FAILED"
export function postUserDetailsFailed(error) {
    return {
        type: POST_USER_DETAILS_FAILED,
        error
    }
}


export function postDetails(dataToPost) {
    return dispatch => {
        fetch('http://localhost:3001/userDetails', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataToPost)
        })
            .then(response => response.json())
            .then(response => {
                if (response.error) {
                    throw (response.error);
                }
                // here we can check for the response and then change the content based on the requirement.
                // dispatch(postUserDetailsSuccess(response));
                // as of now I dont care about the response
                dispatch(postUserDetailsSuccess());
            })
            .catch(error => {
                dispatch(postUserDetailsFailed(error));
            })
    }
}