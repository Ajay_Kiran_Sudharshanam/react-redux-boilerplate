import {
    TODOS_FORM_DETAILS_CHANGE,
    todosFormDetailsChange,
    VALIDATE_TODOS_FORM_FIELDS,
    validateTodosFormFields,
    ON_SUBMIT_VALIDATE_FIELDS,
    onSubmitValidateFields
} from './todos'


describe('actions/todos', () => {

    it('should create an action to change todo form', () => {
        const changedValue = 'test changed value'
        const expectedAction = {
            type: TODOS_FORM_DETAILS_CHANGE,
            changedValue
        }
        expect(todosFormDetailsChange(changedValue)).toEqual(expectedAction)
    })

    it('should create an action to validate the form fields', () => {
        const fieldName = 'test field name'
        const fieldValue = 'test field value'
        const expectedAction = {
            type: VALIDATE_TODOS_FORM_FIELDS,
            fieldName,
            fieldValue
        }
        expect(validateTodosFormFields(fieldName, fieldValue)).toEqual(expectedAction)
    })

    it('should create an action to validate the form fields onSubmit', () => {
        const error = 'test error'
        const expectedAction = {
            type: ON_SUBMIT_VALIDATE_FIELDS,
            error
        }
        expect(onSubmitValidateFields(error)).toEqual(expectedAction)
    })
})