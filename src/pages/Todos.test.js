import React from 'react';
import { shallow, mount } from 'enzyme';
import { Todos } from './Todos';

const props = {
    isFetching: false,
    formDetails: {
        firstName: "test firstName",
        lastName: "test lastName",
        Email: "test Email",
        contactNumber: "test contactNumber",
    },
    isApiSuccess: false,
    onSubmitForm: jest.fn(),
    errorMessage: {},
    validateFields: jest.fn(),
    todosFormDetailsChange: jest.fn(),
}

it('renders without crashing', () => {
    const component = shallow(<Todos {...props} />);
    expect(component).toBeDefined()
});

it('isFetching === true', () => {
    const componentProps = {
        ...props,
        isFetching: true
    }
    const component = shallow(<Todos {...componentProps} />);
    expect(component).toBeDefined()
});

it('isApiSuccess === true', () => {
    const componentProps = {
        ...props,
        isApiSuccess: true
    }
    const component = shallow(<Todos {...componentProps} />);
    expect(component).toBeDefined()
});

it('onSubmit', () => {
    const component = mount(<Todos {...props} />);

    component
        .find('button')
        .at(0)
        .simulate('click')

    expect(props.onSubmitForm).toBeCalled()
});

it('onDetailsBlur', () => {
    const component = mount(<Todos {...props} />);

    component
        .find('input')
        .at(0)
        .simulate('blur')

    expect(props.validateFields).toBeCalled()
});

it('OnDetailsChange', () => {
    const component = mount(<Todos {...props} />);

    component
        .find('input')
        .at(0)
        .simulate('change')

    expect(props.todosFormDetailsChange).toBeCalled()
});