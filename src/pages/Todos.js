import React from 'react'
import LoadingSpinner from '../components/LoadingSpinner'
import FormSubmitted from '../components/FormSubmitted';

export function Todos({
    isFetching,
    formDetails,
    isApiSuccess,
    onSubmitForm,
    errorMessage,
    validateFields,
    todosFormDetailsChange,
}) {

    const { firstName, lastName, contactNumber, email } = formDetails

    const OnDetailsChange = (event) => {
        todosFormDetailsChange({ [event.target.name]: event.target.value })
        validateFields(event.target.name, event.target.value)
    }

    const onDetailsBlur = (event) => {
        validateFields(event.target.name, event.target.value)
    }

    const onSubmit = (event) => {
        event.preventDefault()
        onSubmitForm(formDetails)
    }

    if (isFetching) {
        return <LoadingSpinner show={true} />
    }

    if (isApiSuccess) {
        return <FormSubmitted />
    }

    return (
        <div className="Todos">
            <div className="Form">
                <h2 className="Title">Concord Portal Form</h2>
                <form>
                    <legend className="Legend">First Name:</legend>
                    <input
                        onBlur={onDetailsBlur}
                        name="firstName"
                        type="text"
                        placeholder="Enter First Name"
                        onChange={OnDetailsChange}
                        value={firstName}
                    />
                    <i className="Error-message">{errorMessage.firstName}</i>
                    <legend className="Legend">Last Name:</legend>
                    <input
                        onBlur={onDetailsBlur}
                        name="lastName"
                        type="text"
                        placeholder="Enter Last Name"
                        onChange={OnDetailsChange}
                        value={lastName}
                    />
                    <i className="Error-message"> {errorMessage.lastName}</i>
                    <legend className="Legend">Email Address:</legend>
                    <input
                        onBlur={onDetailsBlur}
                        name="email"
                        type="text"
                        placeholder="Enter Email Address"
                        onChange={OnDetailsChange}
                        value={email}
                    />
                    <i className="Error-message">{errorMessage.email}</i>
                    <legend className="Legend">Contact Number:</legend>
                    <input
                        onBlur={onDetailsBlur}
                        name="contactNumber"
                        type="text"
                        placeholder="Enter Contact Number"
                        onChange={OnDetailsChange}
                        value={contactNumber}
                    />
                    <i className="Error-message" >{errorMessage.contactNumber}</i>
                    <button type="submit" onClick={onSubmit}>Submit</button>
                </form>
            </div>
        </div>
    )
}
