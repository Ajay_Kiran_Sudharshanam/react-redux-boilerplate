import { connect } from 'react-redux'
import {
    todosFormDetailsChange,
    validateTodosFormFields,
    onSubmitValidateFields,
    getDetails,
    postDetails
} from '../actions/todos'
import { Todos } from '../pages/Todos';
import { isAllFieldsValid, getFormFieldsValidated } from '../utils/sharedUtils'
import { ruleSet } from '../reducers/todos'

export const mapStateToProps = state => ({
    formDetails: state.todos.formDetails,
    errorMessage: state.todos.errorMessage,
    isFetching: state.todos.isFetching,
    isApiSuccess: state.todos.isApiSuccess,
})

export const mapDispatchToProps = dispatch => ({

    getDetails: dispatch(getDetails()),

    todosFormDetailsChange: (changedValue) => {
        dispatch(todosFormDetailsChange(changedValue))
    },

    validateFields: (fieldName, fieldvalue) => {
        dispatch(validateTodosFormFields(fieldName, fieldvalue))
    },

    onSubmitForm: (formDetails) => {
        const getErrorField = getFormFieldsValidated(ruleSet, formDetails, "This")
        if (isAllFieldsValid(getErrorField)) {
            dispatch(onSubmitValidateFields(getErrorField))
        } else {
            dispatch(postDetails(formDetails))
        }
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(Todos)