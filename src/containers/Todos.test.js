import freeze from 'deep-freeze-node'
import { mapStateToProps, mapDispatchToProps } from './Todos'

describe('containers/Todos', () => {
    let state
    let dispatch

    beforeEach(() => {
        dispatch = jest.fn()
        state = freeze({
            todos: {
                formDetails: { test: "test formDetails" },
                errorMessage: { test: "test errorMessage" },
                isFetching: false,
                isApiSuccess: false
            }
        })
    })

    it("mapStateToProps", () => {
        expect.assertions(1)
        const result = mapStateToProps(state)
        expect(result).toMatchSnapshot()
    })

    it("todosFormDetailsChange", () => {
        expect.assertions(1)
        const changedValue = "test changed value"
        mapDispatchToProps(dispatch).todosFormDetailsChange(changedValue)
        expect(dispatch.mock.calls).toMatchSnapshot()
    })
 
    it("validateFields", () => {
        expect.assertions(1)
        const fieldName = 'test field name'
        const fieldValue = 'test field value'
        mapDispatchToProps(dispatch).validateFields(fieldName, fieldValue)
        expect(dispatch.mock.calls).toMatchSnapshot()
    })

    it("onSubmitForm else case", () => {
        expect.assertions(1)
        const formDetails = {
            firstName: "test first name",
            lastName: "test last name",
            email: "test@test.com",
            contactNumber: "1111111111"
        }
        mapDispatchToProps(dispatch).onSubmitForm(formDetails)
        expect(dispatch.mock.calls).toMatchSnapshot()
    })

    it("onSubmitForm if case", () => {
        expect.assertions(1)
        const formDetails = {
            firstName: "test first name",
            lastName: "test last name",
            email: "test email",
            contactNumber: "1111111111"
        }
        mapDispatchToProps(dispatch).onSubmitForm(formDetails)
        expect(dispatch.mock.calls).toMatchSnapshot()
    })
})