import {
    initialState,
    todos,
    TODOS_FORM_DETAILS_CHANGE,
    VALIDATE_TODOS_FORM_FIELDS,
    ON_SUBMIT_VALIDATE_FIELDS,
    FETCH_USER_DETAILS_STARTED,
    FETCH_USER_DETAILS_SUCCESS,
    POST_USER_DETAILS_SUCCESS
} from './todos'

describe('todos todos', () => {
    it('should return the initial state', () => {
        expect(todos(undefined, {})).toEqual({ ...initialState })
    })

    it('should handle TODOS_FORM_DETAILS_CHANGE', () => {
        const expected = {
            changedValue: "test changed value"
        }
        const action = {
            type: TODOS_FORM_DETAILS_CHANGE,
            changedValue: "test changed value"
        }
        expect(todos(expected, action)).toEqual(expected)
    })

    it('should handle VALIDATE_TODOS_FORM_FIELDS', () => {
        const expected = {
            fieldName: "test field name",
            fieldValue: "test field value"
        }
        const action = {
            type: VALIDATE_TODOS_FORM_FIELDS,
            fieldName: "test field name",
            fieldValue: "test field value"
        }
        expect(todos(expected, action)).toEqual(expected)
    })

    it('should handle ON_SUBMIT_VALIDATE_FIELDS', () => {
        const expected = {
            errorMessage: "test error"
        }
        const action = {
            type: ON_SUBMIT_VALIDATE_FIELDS,
            error: "test error"
        }
        expect(todos(expected, action)).toEqual(expected)
    })

    it('should handle FETCH_USER_DETAILS_STARTED', () => {
        const expected = {
            isFetching: true
        }
        const action = {
            type: FETCH_USER_DETAILS_STARTED
        }
        expect(todos(expected, action)).toEqual(expected)
    })

    it('should handle POST_USER_DETAILS_SUCCESS', () => {
        const expected = {
            isApiSuccess: true
        }
        const action = {
            type: POST_USER_DETAILS_SUCCESS
        }
        expect(todos(expected, action)).toEqual(expected)
    })

    it('should handle FETCH_USER_DETAILS_SUCCESS', () => {
        const expected = {
            isFetching: false,
            formDetails: {
                firstName: "test firstName",
                lastName: "",
                email: "test email",
                contactNumber: ""
            }
        }
        const action = {
            type: FETCH_USER_DETAILS_SUCCESS,
            details: {
                firstName: "test firstName",
                lastName: "",
                email: "test email",
                contactNumber: ""
            }
        }
        expect(todos(expected, action)).toEqual(expected)
    })
})