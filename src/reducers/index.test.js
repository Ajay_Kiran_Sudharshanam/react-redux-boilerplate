import { createStore } from 'redux';
import rootReducer from './index';

describe('Store', () => {
    it('should have access to todo reducer', () => {
        const store = createStore(rootReducer, {});
        const actual = store.getState().todos;
        const expected = {
            formDetails: {
                firstName: "",
                lastName: "",
                email: "",
                contactNumber: ""
            },
            errorMessage: {},
            isFetching: false,
            isApiSuccess: false
        };

        expect(actual).toEqual(expected);
    });
});