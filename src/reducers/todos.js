import {
    TODOS_FORM_DETAILS_CHANGE,
    VALIDATE_TODOS_FORM_FIELDS,
    ON_SUBMIT_VALIDATE_FIELDS,
    POST_USER_DETAILS_SUCCESS,
    FETCH_USER_DETAILS_STARTED,
    FETCH_USER_DETAILS_SUCCESS
} from '../actions/todos'
import { validate } from '../utils/sharedUtils';

export const ruleSet = {
    firstName: ["required", "min:3"],
    lastName: ["required", "min:3"],
    email: ["required", "email"],
    contactNumber: ["required", "min:10", "max:10"]
}

export const initialState = {
    formDetails: {
        firstName: "",
        lastName: "",
        email: "",
        contactNumber: ""
    },
    errorMessage: {},
    isFetching: false,
    isApiSuccess: false
}

export function todos(state = initialState, action) {
    switch (action.type) {

        case TODOS_FORM_DETAILS_CHANGE:
            return {
                ...state,
                formDetails: {
                    ...state.formDetails,
                    ...action.changedValue
                }
            }

        case VALIDATE_TODOS_FORM_FIELDS:
            return {
                ...state,
                errorMessage: {
                    ...state.errorMessage,
                    [action.fieldName]: validate(
                        ruleSet[[action.fieldName]],
                        action.fieldValue,
                        "This"
                    )
                }
            }

        case ON_SUBMIT_VALIDATE_FIELDS:
            return {
                ...state,
                errorMessage: {
                    ...state.errorMessage,
                    ...action.error
                }
            }

        case FETCH_USER_DETAILS_STARTED:
            return {
                ...state,
                isFetching: true
            }

        case FETCH_USER_DETAILS_SUCCESS:
            return {
                ...state,
                formDetails: action.details,
                isFetching: false
            }

        case POST_USER_DETAILS_SUCCESS:
            return {
                ...state,
                isApiSuccess: true
            }

        default:
            return state
    }
}

export default todos